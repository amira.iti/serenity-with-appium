Feature: moneFyTest

  Scenario: add income and make sure that it added
    Given the user opened the app
    When user clicks in income button
    And user enter the balance and choose category
    Then the balance will be added and the user will be redirected to the dashboard


    Scenario: cancel adding income
      Given the user opened the app
      When user clicks in income button
      And user enter the balance and choose category
      And from the dashboard click on cancel from the snackBar
