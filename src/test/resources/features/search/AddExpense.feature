Feature: Add Expense

  Scenario: user adding expense
    Given the user opened the app
    When user click on Expense button
    And user enter the amount of expense
    And user choose the category
    Then the expense will be added and the user will be redirected to the dashboard



  Scenario: user adding expense from dashboard category icons
    Given the user opened the app
    When user choose category
    And user enter the amount of expense
    And user clicks on Add -category name-
    Then the expense will be added and the user will be redirected to the dashboard
