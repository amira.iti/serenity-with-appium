Feature: User transfer money (Cash/Payment card)

  Scenario: user transfer money from cash to Payment card
    Given the user opened the app
    When user clicks on menu and choose Accounts
    And user clicks on transfer
    And user check that the transfer will be from Cash to Payment card
    And user enter the amount of transfer money
    Then user clicks on Add transfer and will be redirected to the dashboard


  Scenario: user transfer money from Payment card to cash
    Given the user opened the app
    When user clicks on menu and choose Accounts
    And user clicks on transfer
    And user check that the transfer will be from Payment card to Cash
    And user enter the amount of transfer money
    Then user clicks on Add transfer and will be redirected to the dashboard



