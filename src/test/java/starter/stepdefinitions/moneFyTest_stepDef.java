package starter.stepdefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.TestCases.*;

public class moneFyTest_stepDef {

@Steps
    addIncome add;
    CancelIncome cancel;
    AddExpense expense;
    TransferMoney transfer;
    Search_implementation search;
    Currency currency;


    @Given("the user opened the app")
    public void the_user_opened_the_app() {

    }
    @When("user clicks in income button")
    public void user_clicks_in_income_button() {
    add.click_on_income_button();
    }
    @When("user enter the balance and choose category")
    public void user_enter_the_balance_and_choose_category() {
        add.enter_the_balance_and_choose_category();
    }
    @Then("the balance will be added and the user will be redirected to the dashboard")
    public void the_balance_will_be_added_and_the_user_will_be_redirected_to_the_dashboard() {

        add.balance_will_be_added_and_the_user_will_be_redirected_to_the_dashboard();
    }



    @When("from the dashboard click on cancel from the snackBar")
    public void from_the_dashboard_click_on_cancel_from_the_snack_bar() {
        cancel.click_on_cancel_button();
    }
    @Then("the balance will be restore to Zero")
    public void the_balance_will_be_restore_to_zero() {
        cancel.check_that_balance_will_be_restore_to_zero();
    }



    @When("user click on Expense button")
    public void user_click_on_expense_button() {
        expense.click_on_expense_button();
    }
    @When("user enter the amount of expense")
    public void user_enter_the_amount_of_expense() {
        expense.the_amount_of_expense();
    }
    @When("user choose the category")
    public void user_choose_the_category() {
        expense.user_choose_the_category();
    }
    @Then("the expense will be added and the user will be redirected to the dashboard")
    public void the_expense_will_be_added_and_the_user_will_be_redirected_to_the_dashboard() {

    }



    @When("user clicks on menu and choose Accounts")
    public void user_clicks_on_menu_and_choose_accounts() {
        transfer.clicks_on_menu_and_choose_accounts();
    }
    @When("user clicks on transfer")
    public void user_clicks_on_transfer() {
        transfer.user_clicks_on_transfer();
    }
    @When("user check that the transfer will be from Cash to Payment card")
    public void user_check_that_the_transfer_will_be_from_cash_to_payment_card() {
        transfer.check_that_the_transfer_will_be_from_cash_to_payment_card();
    }
    @When("user enter the amount of transfer money")
    public void user_enter_the_amount_of_transfer_money() {
        transfer.user_enter_the_amount_of_transfer_money();
    }
    @Then("user clicks on Add transfer and will be redirected to the dashboard")
    public void user_clicks_on_add_transfer_and_will_be_redirected_to_the_dashboard() throws InterruptedException {
        transfer.clicks_on_add_transfer_and_will_be_redirected_to_the_dashboard();
        Thread.sleep(3000);
    }


    @When("user check that the transfer will be from Payment card to Cash")
    public void user_check_that_the_transfer_will_be_from_payment_card_to_cash() {
        transfer.check_that_the_transfer_will_be_from_payment_card_to_cash();

    }


    @When("user choose category")
    public void user_choose_category() {

        expense.user_choose_category();
    }
    @When("user clicks on Add -category name-")
    public void user_clicks_on_add_category_name() {

        expense.user_clicks_on_add_category_name();
    }







    @Given("the user open the app")
    public void the_user_open_the_app() {

    }

    @When("the user click on search button")
    public void the_user_click_on_search_button() {
        search.the_user_click_on_search_button();

    }

    @When("the user enter a new record")
    public void the_user_enter_a_new_record() {
        search.the_user_enter_a_new_record();
    }

    @When("the user choose the match record")
    public void the_user_choose_the_match_record() {
        search.the_user_choose_the_match_record();
    }

    @Then("the app will redirect to match record page")
    public void the_app_will_redirect_to_match_record_page() {
        search.the_app_will_redirect_to_match_record_page();
    }

    @When("the user click on menu button")
    public void theUserClickOnMenuButton() {
        currency.theUserClickOnMenuButton();
    }

    @And("the user choose settings")
    public void theUserChooseSettings() {
        currency.theUserChooseSettings();
    }

    @And("the user click on currency")
    public void theUserClickOnCurrency() {
        currency.theUserClickOnCurrency();
    }

    @And("the user enter the currency name")
    public void theUserEnterTheCurrencyName() {
        currency.theUserEnterTheCurrencyName();
    }

    @Then("the user choose the currency")
    public void theUserChooseTheCurrency() {
        currency.theUserChooseTheCurrency();
    }


    @And("the user enter the currency code")
    public void theUserEnterTheCurrencyCode() {
        currency.theUserEnterTheCurrencyCode();
    }


}
