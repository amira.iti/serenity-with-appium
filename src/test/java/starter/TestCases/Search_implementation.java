package starter.TestCases;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


@RunWith(SerenityRunner.class)
public class Search_implementation extends PageObject {



    @Step
    public void the_user_click_on_search_button() {
        element(By.id("com.monefy.app.lite:id/menu_search")).waitUntilVisible().click();
    }

    @Step
    public void the_user_enter_a_new_record() {
        element(By.id("com.monefy.app.lite:id/et_search")).waitUntilVisible().sendKeys("F");

    }

    @Step
    public void the_user_choose_the_match_record() {
        element(By.id("com.monefy.app.lite:id/pts_main")).waitUntilVisible().click();
    }


    @Step
    public void the_app_will_redirect_to_match_record_page() {
        element(By.id("com.monefy.app.lite:id/empty_results_image")).waitUntilVisible();
    }


}
