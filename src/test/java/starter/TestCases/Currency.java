package starter.TestCases;


import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class Currency extends PageObject {
    @Step
    public void theUserClickOnMenuButton() {
        element(By.id("com.monefy.app.lite:id/overflow")).waitUntilVisible().click();
    }

    @Step
    public void theUserChooseSettings() {
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout[1]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView[13]")).waitUntilVisible().click();
    }

    @Step
    public void theUserClickOnCurrency() {
        element(By.id("com.monefy.app.lite:id/currency_name")).waitUntilVisible().click();
    }

    @Step
    public void theUserEnterTheCurrencyName() {
        element(By.id("com.monefy.app.lite:id/search_src_text")).waitUntilVisible().sendKeys("Egyptian");
    }

    @Step
    public void theUserChooseTheCurrency() {

        element(By.id("com.monefy.app.lite:id/relativeLayoutManageCategoriesListItem")).waitUntilVisible().click();
    }

    @Step
    public void theUserEnterTheCurrencyCode(){
        element(By.id("com.monefy.app.lite:id/search_src_text")).waitUntilVisible().sendKeys("EGP");
    }

}
