package starter.TestCases;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

@RunWith(SerenityRunner.class)
public class CancelIncome extends PageObject {


    @Step
    public void click_on_cancel_button(){

       // element(By.id("com.monefy.app.lite:id/piegraph")).waitUntilVisible();
        element(By.id("com.monefy.app.lite:id/snackbar_action")).waitUntilVisible().click();

    }

    @Step
    public void check_that_balance_will_be_restore_to_zero(){

        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout")).waitUntilVisible();
    }

}
