package starter.TestCases;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

@RunWith(SerenityRunner.class)
public class addIncome extends PageObject {

@Step
    public void click_on_income_button(){

    element(By.id("com.monefy.app.lite:id/income_button_title")).waitUntilVisible().click();

}
@Step
    public void enter_the_balance_and_choose_category(){

    element(By.id("com.monefy.app.lite:id/buttonKeyboard2")).waitUntilVisible().click();
    element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();
    element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();
    element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();
    element(By.id("com.monefy.app.lite:id/keyboard_action_button")).waitUntilVisible().click();
    element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.ImageView")).waitUntilVisible().click();

}
@Step
    public void balance_will_be_added_and_the_user_will_be_redirected_to_the_dashboard(){

    element(By.id("com.monefy.app.lite:id/piegraph")).waitUntilVisible();

}
}
