package starter.TestCases;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

@RunWith(SerenityRunner.class)

public class AddExpense extends PageObject {

    @Step
    public void click_on_expense_button(){

        element(By.id("com.monefy.app.lite:id/expense_button_title")).waitUntilVisible().click();

    }
    @Step
    public void the_amount_of_expense(){
        element(By.id("com.monefy.app.lite:id/buttonKeyboard1")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();

    }
    @Step
    public void user_choose_the_category(){
        element(By.id("com.monefy.app.lite:id/keyboard_action_button")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.FrameLayout[6]/android.widget.LinearLayout/android.widget.ImageView")).waitUntilVisible().click();

    }

    @Step
    public void user_choose_category(){

        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView[8]")).waitUntilVisible().click();

    }

    @Step
    public void user_clicks_on_add_category_name(){

        element(By.id("com.monefy.app.lite:id/keyboard_action_button")).waitUntilVisible().click();
    }

}
