package starter.TestCases;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

@RunWith(SerenityRunner.class)

public class TransferMoney extends PageObject {

    @Step
    public void clicks_on_menu_and_choose_accounts(){

        element(By.id("com.monefy.app.lite:id/overflow")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout[1]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView[6]")).waitUntilVisible().click();
    }
    @Step
    public void user_clicks_on_transfer(){
        element(By.xpath("//android.widget.ListView[@content-desc=\"List of accounts\"]/android.widget.LinearLayout[2]/android.widget.TextView")).waitUntilVisible().click();

    }

    @Step
    public void check_that_the_transfer_will_be_from_cash_to_payment_card(){

        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Spinner[1]/android.widget.LinearLayout/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Spinner[2]/android.widget.LinearLayout/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout")).waitUntilVisible().click();

    }

    @Step
    public void user_enter_the_amount_of_transfer_money(){
        element(By.id("com.monefy.app.lite:id/amount_text")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/buttonKeyboard1")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/buttonKeyboard0")).waitUntilVisible().click();

    }
    @Step
    public void clicks_on_add_transfer_and_will_be_redirected_to_the_dashboard(){
        element(By.id("com.monefy.app.lite:id/keyboard_action_button")).waitUntilVisible().click();
        element(By.id("com.monefy.app.lite:id/piegraph")).waitUntilVisible();
    }

    @Step
    public void check_that_the_transfer_will_be_from_payment_card_to_cash(){

        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Spinner[1]/android.widget.LinearLayout/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Spinner[2]/android.widget.LinearLayout/android.widget.LinearLayout")).waitUntilVisible().click();
        element(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout")).waitUntilVisible().click();


    }
}
