Feature: Search

  Scenario: check that the search button works successfully
    Given the user open the app
    When the user click on search button
    And the user enter a new record
    And the user choose the match record
    Then the app will redirect to match record page

