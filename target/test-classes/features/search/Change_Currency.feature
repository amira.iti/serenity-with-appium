Feature: Change currency

  Scenario: Change currency by searching with name
    Given the user open the app
    When the user click on menu button
    And the user choose settings
    And the user click on currency
    And the user enter the currency name
    Then the user choose the currency


  Scenario: Change currency by searching with Code
    Given the user open the app
    When the user click on menu button
    And the user choose settings
    And the user click on currency
    And the user enter the currency code
    Then the user choose the currency