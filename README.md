# Mobile automation using Appium, Serenity and Cucumber

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.

Serenity strongly encourages good test automation design, and supports several design patterns, including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports Cucumber 6.x.


# Why Serenity Framework with Cucumber

- Serenity BDD provides strong support for different types of automated acceptance testing, including:

- Rich built-in support for web testing with Selenium.

- REST API testing with RestAssured.

- Highly readable, maintainable and scalable automated testing with the Screenplay pattern.

# How to install

### Java Installation

```
install Java 
and insert it in environment 
JAVA_HOME=C:\Program Files\Java\jdk-17.0.1
```



### Install Android Studio
 
```
insert it in environment 
ANDROID_HOME=C:\Users\"username"\AppData\Local\Android\Sdk
```


### Install ADB 

Form the following Link https://adb.clockworkmod.com/
And insert it in environment ADB=C:\Users\"username"\AppData\Local\Android\Sdk\platform-tools

Check that you installed successfully

Connect your device by using usb

- Open Cmd
- Write the following Command ⎝ “adb devices”


## Install Vysor App in both Pc and Mobile

From the following Link 

https://www.vysor.io/



## Install Appium GUI Desktop tool



## Install Node Js

From the following Link 
https://nodejs.org/en/download/



## To check that node js installed successfully

- Open CMD
- Write the following command “node --version”



## To Get Appium

- Open CMD


- Write the following command 
      “npm install -g Appium”



## Install Appium inspector

- Open Appium Server GUI
-  Click on search icon 
-  GitHub page will be opened 
-  From installation click on Releases and you 
-     will be redirect to this link
- https://github.com/appium/appium-inspector/releases
- From Assets choose Appium-Inspector-windows-2021.9.2.exe for windows
- From Assets choose Appium-Inspector-mac-2021.9.2.dmg for macOS



## serenity properties 

webdriver.driver=appiumappium.hub=http://127.0.0.1:4723/wd/hubappium.platformName= your platformName 

```
appium.deviceName= your deviceName
appium.appPackage=  to get app package you should download 'apk info'
appium.appActivity=  to get app Activity you should download 'apk info'
appium.platformVersion= your mobile software version
appium.app= the apk path
```


## pom.xml file (dependences)

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.serenitybdd.starter</groupId>
    <artifactId>cucumber-starter</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>Sample Serenity BDD project using Cucumber</name>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <serenity.version>3.0.5</serenity.version>
        <serenity.maven.version>3.0.5</serenity.maven.version>
        <serenity.cucumber.version>3.0.5</serenity.cucumber.version>
        <encoding>UTF-8</encoding>
        <tags></tags>
        <webdriver.base.url></webdriver.base.url>
    </properties>
    <dependencies>
        <!-- https://mvnrepository.com/artifact/io.appium/java-client -->
        <dependency>
            <groupId>io.appium</groupId>
            <artifactId>java-client</artifactId>
            <version>7.3.0</version>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-core</artifactId>
            <version>${serenity.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-cucumber</artifactId>
            <version>${serenity.cucumber.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-screenplay</artifactId>
            <version>${serenity.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-screenplay-webdriver</artifactId>
            <version>${serenity.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-ensure</artifactId>
            <version>${serenity.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <version>1.8.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.8.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.8.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.0.13</version>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.6.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <includes>
                        <include>**/*Test.java</include>
                        <include>**/Test*.java</include>
                        <include>**/*TestSuite.java</include>
                        <include>**/When*.java</include>
                    </includes>
                    <systemPropertyVariables>
                        <webdriver.base.url>${webdriver.base.url}</webdriver.base.url>
                    </systemPropertyVariables>
                    <parallel>classes</parallel>
                    <parallel>methods</parallel>
                    <useUnlimitedThreads>true</useUnlimitedThreads>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>net.serenity-bdd.maven.plugins</groupId>
                <artifactId>serenity-maven-plugin</artifactId>
                <version>${serenity.maven.version}</version>
                <configuration>
                    <tags>${tags}</tags>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>net.serenity-bdd</groupId>
                        <artifactId>serenity-core</artifactId>
                        <version>${serenity.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>serenity-reports</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```
## To Run


- mvn clean verify

## Result 
- Please check target>site>index.html file 









